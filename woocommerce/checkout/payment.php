<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<div id="payment" class="woocommerce-checkout-payment">
	<?php if ( WC()->cart->needs_payment() ) : ?>
		
			<?php
				if ( ! empty( $available_gateways ) ) {
					echo "<div id=\"tab\"> <div class=\"tab\">";
					$i = 1;
					foreach ( $available_gateways as $gateway ) {
						//wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						?>
						
							<a href="#">
<input id="payment_method_<?php echo $gateway->id; ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />
	<?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?></a>
<?php
					}
					echo "</div>";
					$ii = 1;
					foreach ( $available_gateways as $gateway ) {
						//wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						?>
						<div class="tabContent">
	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<?php $gateway->payment_fields(); ?>
		</div>
	<?php endif; ?>
</div>

<?php
					} echo "</div>";

				} else {
					echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : __( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>';
				}
			?>
		</ul>
	<?php endif; ?>
	<div class="form-row place-order">
		<noscript>
			<?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>" />
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
	</div>
</div>
<style type="text/css">

	.tab a input{display: none;}
</style>
<script type="text/javascript">
(function($) {
$(document).ready(function() {

	var tab = $('.tab a'),
		content = $('.tabContent'),
		divs = $('.tabContent div');
	
	// ilk tab'a aktif sÄ±nÄ±fÄ±nÄ± ata
	tab.filter(':first').addClass('aktif');
	
	// ilk iÃ§erik hariÃ§ diÄŸerlerini gizle
	content.filter(':not(:first)').hide();
	
	// taba tÄ±klandÄ±ÄŸÄ±nda!
	tab.click(function(){
		var indis = $(this).index();
		tab.removeClass('aktif').eq(indis).addClass('aktif');
		$( this ).children().attr( 'checked', true );
		content.hide().eq(indis).fadeIn(500);
		divs.show();
		return false;
	});
});
}) (jQuery);
</script>
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
