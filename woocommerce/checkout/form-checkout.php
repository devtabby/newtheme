<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );


// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div id="order_review" class="woocommerce-checkout-review-order">
		<?php wc_get_template( 'checkout/review-order.php' ); ?>
		<div class="sepetalti" style="float:left;width:70%;padding:5px 15%;text-align:center;background:#f8f8f8;border:1px solid #e0e0e0;">
					<a href="<?php echo home_url();?>/sepetim" class="sepetbuton">SEPETİM</a>
<a href="<?php echo home_url();?>" class="sepetbuton">ALIŞVERİŞE DEVAM ET</a>
		</div>

	</div>
			<div class="col-1">
			    <a class="uyeyonlendir" href="/giris-uyelik">Adres Oluşturabilmeniz ve Alışveriş Yapabilmeniz İçin Önce Üyemiz Olmalısınız. Üye Olmak İçin Buraya Tıklayınız!</a>
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
			<div class="barss">
   				 <h3>2-Kargo ve Ücretler</h3>
			</div>
			<div class="kargolar">
				<?php 
				$price = WC()->cart->cart_contents_total;
				if($price<75){
					echo "<p>Aras Kargo - <b>5,80TL</b></p>";
					 
				}else{
					echo "<p>Aras Kargo - <b>0,00TL</b></p>";
				}
				?>
				
			</div>
			<div class=""></div>
			<div class="barss">
   				 <h3>3-Ödeme Seçenekleri</h3>
			</div>
			<div class="odemeler">
				<?php wc_get_template( 'checkout/payment.php' ); ?>
				
			</div>

	
		</div>


	<?php endif; ?>





	

</form>

<?php  do_action( 'woocommerce_after_checkout_form', $checkout );  ?>

<?php add_action( 'woocommerce_review_order_before_payment', 'woocommerce_checkout_coupon_form' );  ?>

